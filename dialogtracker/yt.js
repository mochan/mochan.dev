// Get reference to iframe and input elements
const player = document.getElementById("player");
const progress = document.getElementById("progress");

// Create YouTube player
let ytplayer;
function onYouTubeIframeAPIReady() {
  ytplayer = new YT.Player("player", {
    events: {
      onReady: onPlayerReady
    }
  });
}

// When player is ready, update progress bar
function onPlayerReady(event) {
  let playbackProgress;

  setInterval(function() {
    playbackProgress = ytplayer.getCurrentTime() / ytplayer.getDuration();
    progress.value = (playbackProgress * 100).toFixed(2);
  }, 100);
}
