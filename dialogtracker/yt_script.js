// Function that converts seconds to hours:minutes:seconds
function secondsToHms(seconds) {
  if (!seconds) return '00:00:00';

  let duration = seconds;
  let hours = duration / 3600;
  duration = duration % (3600);

  let min = parseInt(duration / 60);
  duration = duration % (60);

  let sec = parseInt(duration);

  if (sec < 10) {
    sec = `0${sec}`;
  }
  if (min < 10) {
    min = `0${min}`;
  }

  if (parseInt(hours, 10) > 0) {
    return `${parseInt(hours, 10)}:${min}:${sec}`;
  } else if (min == 0) {
    return `00:00:${sec}`;
  } else {
    return `00:${min}:${sec}`;
  }
}

// Global data
let json_data;

document.addEventListener("DOMContentLoaded", function () {
  fetch('645.corrected.json')
    .then(response => response.json())
    .then(data => {
      json_data = data;

      const container = document.getElementById('container');
      const audioElement = document.getElementById('audioElement');

      let speakers = data['speakers'];

      data['dialogs'].forEach((dialog, index) => {
        const div = document.createElement('div');
        div.id = `dialog-${index}`;

        let speaker = speakers[dialog.speaker];
        let start_time = secondsToHms(dialog.start);
        let end_time = secondsToHms(dialog.end);

        div.innerHTML = `<b>${speaker}</b> <i>(${start_time} - ${end_time})</i>:`;
        container.appendChild(div);

        let dialog_text = '';
        dialog.words.forEach((word, jindex) => {
          dialog_text += `${word.word} `;

          const word_span = document.createElement('span');
          word_span.id = `word-${index}.${jindex}`;
          word_span.innerHTML = `${word.word}`;
          div.appendChild(word_span);
        });
      });
    })
    .catch(error => console.error('Error:', error));
});
